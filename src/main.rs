extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate reqwest;
extern crate clap;

mod speed;
mod free;
mod connect;

use clap::{Arg, App, SubCommand};





fn main() {
    let matches = App::new("SS 梯子")
        .version("0.1.0")
        .author("zxk7516")
        .arg(Arg::with_name("port")
            .short("c").long("port").value_name("PORT").help("请选择SS 本地代理的端口号")
            .default_value("1080").takes_value(true)
        ).subcommand(SubCommand::with_name("test").about("测试免费节点的数目"))
        .subcommand(SubCommand::with_name("demo").about("连接固定节点"))
        .get_matches();

    let port = matches.value_of("port").unwrap_or("1080");
    if let Some(_) = matches.subcommand_matches("test") {
        speed::speed();
    }else if let Some(_) = matches.subcommand_matches("demo") {

    }else {
        free::free();
    }


}
